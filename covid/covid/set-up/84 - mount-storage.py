# Databricks notebook source
# MAGIC %md
# MAGIC ## Mount the following data lake storage gen2 containers
# MAGIC 1. raw
# MAGIC 2. processed
# MAGIC 3. lookup

# COMMAND ----------

# MAGIC %md
# MAGIC ### Set-up the configs
# MAGIC #### Please update the following 
# MAGIC - application-id
# MAGIC - service-credential
# MAGIC - directory-id

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
           "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
           "fs.azure.account.oauth2.client.id": "883b8d89-585a-4ed1-bfa6-7a34cbfe9ad8",
           "fs.azure.account.oauth2.client.secret": "ndx8Q~.~privfzLQTm4PdR4DhUm-NeSn22vsFcYQ",
           "fs.azure.account.oauth2.client.endpoint": "https://login.microsoftonline.com/b376ee7c-c22b-4600-817b-de91ab04a4bb/oauth2/token"}

# COMMAND ----------

# MAGIC %md
# MAGIC ### Mount the raw container
# MAGIC #### Update the storage account name before executing

# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://raw@covid2019reportingdl.dfs.core.windows.net/",
  mount_point = "/mnt/covid2019reportingdl/raw",
  extra_configs = configs)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Mount the processed container
# MAGIC #### Update the storage account name before executing

# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://processed@covid2019reportingdl.dfs.core.windows.net/",
  mount_point = "/mnt/covid2019reportingdl/processed",
  extra_configs = configs)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Mount the lookup container
# MAGIC #### Update the storage account name before executing

# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://lookup@covid2019reportingdl.dfs.core.windows.net/",
  mount_point = "/mnt/covid2019reportingdl/lookup",
  extra_configs = configs)

# COMMAND ----------



# COMMAND ----------


